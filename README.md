# README #

### What is this repository for? ###

* This is the first exercise of StyleSage's python test.

### What the code supose to do?
* The code takes an integer and return a string representation of that integer with commas separating groups of 3 digits.

### How do I try it? ###

* Just execute like this:

```shell
python main.py <integer>
```
