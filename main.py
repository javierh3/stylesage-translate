import sys
from translate import translate

if len(sys.argv) == 1 or len(sys.argv[1:]) > 1:
    print("Error, usage python main.py <number>")
else:
    try:
        number = int(sys.argv[1])
        print(translate(number))
    except ValueError as e:
        print("Error, usage python main.py <number>")
        print("<number> must be an integer")
